package com.oldmutual.omjavainsights.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.oldmutual.omjavainsights.controllers.di.EventServicesDI;
import com.oldmutual.omjavainsights.dtos.inbound.event.NewBusinessDTO;
import com.oldmutual.omjavainsights.dtos.inbound.event.ScreeningResultDTO;
import com.oldmutual.omjavainsights.dtos.outbound.event.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/event")
public class EventController {

    private final EventServicesDI eventServicesDI;

    public EventController(EventServicesDI eventServicesDI) {
        this.eventServicesDI = eventServicesDI;
    }


    @PostMapping
    @ResponseBody
    private ResponseEntity<String> getHeartBeat(){

        return new ResponseEntity<>(this.toString(), HttpStatus.OK);
    }

    @PostMapping(value = {"/RiskRatingInProgress", "/riskratinginprogress", "/riskRatingInProgress"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> riskRatingInProgress(@RequestBody Object object) throws InterruptedException, JsonProcessingException {

        var result = eventServicesDI.rrInProgressService.processRiskRatingInProgress(object);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = {"/ScreeningInProgress","/screeninginprogress","/screeningInProgress"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> screeningInProgress(@RequestBody ScreeningInProgressDTO dto) throws InterruptedException {
        var result = eventServicesDI.screeningInProgressService.processScreeningInProgress(dto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = {"/NewRequirement","/newrequirement","/newRequirement"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> newRequirementInProgress(@RequestBody NewRequirementDTO dto) throws InterruptedException {
        var result = eventServicesDI.newRequirementService.processNewRequirement(dto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = {"/UpdateRequirement","/updaterequirement","/updateRequirement"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> updateRequirement(@RequestBody UpdateRequirementDTO dto) throws InterruptedException {
        var result = eventServicesDI.updateRequirementService.processUpdateRequirement(dto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = {"/Screening","/screening"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> screeningRequest(@RequestBody ScreeningRequestDTO dto) throws InterruptedException {
        var result = eventServicesDI.screeningService.processScreening(dto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = {"/RiskRatingResultEvent","/riskratingresultevent", "/riskRatingResultEvent"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> riskRatingResultEvent(@RequestBody Object dto) throws InterruptedException, JsonProcessingException {
        var result = eventServicesDI.riskRatingResultEventService.processRiskRatingResultEvent(dto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = {"/NewBusinessAPIComplete","/newbusinessapicomplete", "/newBusinessApiComplete"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> newBusinessAPIComplete(@RequestBody NewBusinessDTO dto) throws InterruptedException {
        var result = eventServicesDI.newBusinessAPICompleteService.processNewBusinessAPIComplete(dto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = {"/ScreeningResult","/screeningresult", "/screeningResult"}, consumes = "application/json")
    @ResponseBody
    private ResponseEntity<String> screeningResult(@RequestBody ScreeningResultDTO dto) throws InterruptedException {
        var result = eventServicesDI.screeningResultService.processScreeningResult(dto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


}
