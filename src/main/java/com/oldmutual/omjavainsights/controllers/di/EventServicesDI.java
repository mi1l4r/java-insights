package com.oldmutual.omjavainsights.controllers.di;

import com.oldmutual.omjavainsights.services.event.*;
import org.springframework.stereotype.Component;

@Component
public class EventServicesDI {

    public final RiskRatingInProgressService rrInProgressService;
    public final ScreeningInProgressService screeningInProgressService;
    public final NewRequirementService newRequirementService;
    public final UpdateRequirementService updateRequirementService;
    public final ScreeningRequestService screeningService;
    public final RiskRatingResultEventService riskRatingResultEventService;
    public final NewBusinessAPICompleteService newBusinessAPICompleteService;
    public final ScreeningResultService screeningResultService;

    public EventServicesDI(RiskRatingInProgressService rrInProgressService,
                           ScreeningInProgressService screeningInProgressService,
                           NewRequirementService newRequirementService,
                           UpdateRequirementService updateRequirementService,
                           ScreeningRequestService screeningService,
                           RiskRatingResultEventService riskRatingResultEventService,
                           NewBusinessAPICompleteService newBusinessAPICompleteService,
                           ScreeningResultService screeningResultService) {
        this.rrInProgressService = rrInProgressService;
        this.screeningInProgressService = screeningInProgressService;
        this.newRequirementService = newRequirementService;
        this.updateRequirementService = updateRequirementService;
        this.screeningService = screeningService;
        this.riskRatingResultEventService = riskRatingResultEventService;
        this.newBusinessAPICompleteService = newBusinessAPICompleteService;
        this.screeningResultService = screeningResultService;
    }
}
