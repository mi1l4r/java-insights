package com.oldmutual.omjavainsights.dtos.inbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oldmutual.omjavainsights.dtos.inbound.model.BusinessTransactionDTO;
import com.oldmutual.omjavainsights.dtos.inbound.model.PartyDTO;
import lombok.Data;

import java.util.List;

@Data
public class NewBusinessAPICompleteDTO {

    @JsonProperty("BusinessTransaction")
    private BusinessTransactionDTO businessTransaction;

    @JsonProperty("BusinessTransactionStartTime")
    private String businessTransactionStartTime;

    @JsonProperty("ContractIDs")
    private List<String> contractIds;

    @JsonProperty("PartyIDs")
    private List<PartyDTO> partyIds;

    @JsonProperty("SourceSystem")
    private String sourceSystem;

    @JsonProperty("Status")
    private String status;
}
