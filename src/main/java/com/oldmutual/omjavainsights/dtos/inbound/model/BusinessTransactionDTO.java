package com.oldmutual.omjavainsights.dtos.inbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BusinessTransactionDTO {

    @JsonProperty("Key")
    private String key;

    @JsonProperty("Type")
    private String type;
}
