package com.oldmutual.omjavainsights.dtos.inbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PartyDTO {

    @JsonProperty("PartyID")
    private String partyId;

    @JsonProperty("PartyType")
    private String partyType;
}
