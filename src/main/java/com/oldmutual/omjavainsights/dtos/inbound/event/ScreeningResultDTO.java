package com.oldmutual.omjavainsights.dtos.inbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ScreeningResultDTO {

    @JsonProperty("EventType")
    private String eventType;

    @JsonProperty("EventDate")
    private String eventDate;

    @JsonProperty("InitiatingSystem")
    private String initiatingSystem;

    @JsonProperty("ScreeningType")
    private String screeningType;

    @JsonProperty("PartyId")
    private String partyId;

    @JsonProperty("PartyType")
    private String partyType;

    @JsonProperty("SanctionsOutcome")
    private String sanctionsOutcome;

    @JsonProperty("DPIPOutcome")
    private String dpipOutcome;

    @JsonProperty("FPPOOutcome")
    private String fppoOutcome;

    @JsonProperty("AMEOutcome")
    private String ameOutcome;

    @JsonProperty("AMLOutcome")
    private String amlOutcome;

    @JsonProperty("PartyScreeningOutcome")
    private String partyScreeningOutcome;

    @JsonProperty("RCADPIPOutcome")
    private String rcadpipOutcome;

    @JsonProperty("RCAFPPOOutcome")
    private String rcafppoOutcome;

    @JsonProperty("StopTransaction")
    private String stopTransaction;

    @JsonProperty("AccountableCountry")
    private String accountableCountry;

    @JsonProperty("CorrelationId")
    private String correlationId;

    @JsonProperty("ScreeningStatus")
    private String screeningStatus;

    @JsonProperty("EventId")
    private String eventId;
}
