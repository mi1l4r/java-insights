package com.oldmutual.omjavainsights.dtos.inbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EventDTO {

    @JsonProperty("NewBusinessApiComplete")
    private NewBusinessAPICompleteDTO newBusinessAPIComplete;
}
