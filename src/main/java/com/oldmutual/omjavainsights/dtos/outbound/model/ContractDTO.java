package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ContractDTO {

    @JsonProperty("AccountableInstitution")
    private String accountableInstitution;

    @JsonProperty("BusinessUnit")
    private String businessUnit;

    @JsonProperty("ContractRoles")
    private List<ContractRoleDTO> contractRoles;
}
