package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NewRequirementDTO {

    @JsonProperty("EventType")
    private String eventType;

    @JsonProperty("InitiatingSystem")
    private String initiatingSystem;

    @JsonProperty("EventDate")
    private String eventDate;

    @JsonProperty("CorrelationId")
    private String correlationId;

    @JsonProperty("PartyId")
    private String partyId;

    @JsonProperty("RequirementCategory")
    private String requirementCategory;

    @JsonProperty("RequirementType")
    private String requirementType;

    @JsonProperty("RequirementSubType")
    private String requirementSubType;

    @JsonProperty("Regulation")
    private String regulation;

    @JsonProperty("Adjacency")
    private String adjacency;

    @JsonProperty("RequirementStatus")
    private String requirementStatus;

    @JsonProperty("Country")
    private String country;

    @JsonProperty("EventId")
    private String eventId;
}
