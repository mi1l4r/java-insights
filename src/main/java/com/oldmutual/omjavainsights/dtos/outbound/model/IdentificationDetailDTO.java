package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class IdentificationDetailDTO {

    @JsonProperty("IDNumber")
    private String idNumber;

    @JsonProperty("IDType")
    private String idType;

    @JsonProperty("IDCountry")
    private String idCountry;

    @JsonProperty("ModifiedOn")
    private String modifiedOn;
}
