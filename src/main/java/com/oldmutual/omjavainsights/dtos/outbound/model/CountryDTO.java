package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CountryDTO {

    @JsonProperty("countryCode")
    private String countryCode;

    @JsonProperty("countryClassification")
    private String countryClassification;
}
