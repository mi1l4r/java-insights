package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RiskRatingResponseDTO {

    @JsonProperty("CorrelationId")
    private String correlationId;

    @JsonProperty("PartyId")
    private String partyId;

    @JsonProperty("PartyType")
    private String partyType;

    @JsonProperty("Country")
    private String country;

    @JsonProperty("DateLastRiskRated")
    private String dateLastRiskRated;

    @JsonProperty("PartyRiskScore")
    private Integer partyRiskScore;

    @JsonProperty("PrimaryRiskScore")
    private Integer primaryRiskScore;

    @JsonProperty("ProductRiskScore")
    private Integer productRiskScore;

    @JsonProperty("ResponsibleAI")
    private String responsibleAI;

    @JsonProperty("RiskRatingCategory")
    private String riskRatingCategory;

    @JsonProperty("SecondaryRiskScore")
    private Integer secondaryRiskScore;

    @JsonProperty("TotalRiskScore")
    private Integer totalRiskScore;

    @JsonProperty("RiskRatingDetail")
    private RiskRatingDetailDTO riskRatingDetail;

    @JsonProperty("RiskRatingDetailMetadata")
    private String riskRatingDetailMetaData;
}
