package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RiskRatingResultDTO {

    @JsonProperty("InitiatingSystem")
    private String initiatingSystem;

    @JsonProperty("EventType")
    private String eventType;

    @JsonProperty("EventDate")
    private String eventDate;

    @JsonProperty("RiskRatingResponse")
    private RiskRatingResponseDTO riskRatingResponse;

    @JsonProperty("EventId")
    private String eventId;
}
