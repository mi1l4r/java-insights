package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RiskRatingDTO {

    @JsonProperty("PrimaryRiskScore")
    private Integer primaryRiskScore;

    @JsonProperty("SecondaryRiskScore")
    private Integer secondaryRiskScore;

    @JsonProperty("TotalRiskScore")
    private Integer totalRiskScore;
}
