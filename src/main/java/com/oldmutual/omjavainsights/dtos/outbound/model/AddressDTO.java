package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AddressDTO {

    @JsonProperty("AddressType")
    private String addressType;

    @JsonProperty("Country")
    private String country;

    @JsonProperty("ModifiedOn")
    private String modifiedOn;
}
