package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UpdateRequirementDTO {

    @JsonProperty("EventType")
    private String eventType;

    @JsonProperty("InitiatingSystem")
    private String initiatingSystem;

    @JsonProperty("CorrelationId")
    private String correlationId;

    @JsonProperty("EventDate")
    private String eventDate;

    @JsonProperty("PartyId")
    private String partyId;

    @JsonProperty("DateFulFilled")
    private String dateFulfilled;

    @JsonProperty("RequirementCategory")
    private String requirementCategory;

    @JsonProperty("RequirementType")
    private String requirementType;

    @JsonProperty("RequirementSubType")
    private String requirementSubType;

    @JsonProperty("RequirementStatus")
    private String requirementStatus;

    @JsonProperty("EventId")
    private String eventId;
}
