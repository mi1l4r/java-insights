package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PartyDTO {

    @JsonProperty("partyID")
    private String partyId;

    @JsonProperty("partyType")
    private String partyType;

    @JsonProperty("idType")
    private String idType;

    @JsonProperty("countryOfIdIssue")
    private CountryDTO countryOfIdIssue;

    @JsonProperty("countryOfBirth")
    private CountryDTO countryOfBirth;

    @JsonProperty("countryOfResidence")
    private CountryDTO countryOfResidence;

    @JsonProperty("nationality")
    private String nationality;

    @JsonProperty("entityRiskScore")
    private Integer entityRiskScore;

    @JsonProperty("productRiskScore")
    private Integer productRiskScore;

    @JsonProperty("primaryRiskScore")
    private Integer primaryRiskScore;

    @JsonProperty("secondaryRiskScore")
    private Integer secondaryRiskScore;

    @JsonProperty("totalRiskScore")
    private Integer totalRiskScore;

    @JsonProperty("ameoutcome")
    private String ameOutcome;

    @JsonProperty("amloutcome")
    private String amlOutcome;

    @JsonProperty("fppooutcome")
    private String fppoOutcome;

    @JsonProperty("rcafppooutcome")
    private String rcafppoOutcome;

    @JsonProperty("sanctionsOutcome")
    private String sanctionsOutcome;

    @JsonProperty("products")
    private List<ProductDTO> products;
}
