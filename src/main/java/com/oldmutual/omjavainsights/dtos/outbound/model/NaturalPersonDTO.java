package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oldmutual.omjavainsights.dtos.outbound.event.RiskRatingDTO;
import lombok.Data;

import java.util.List;

@Data
public class NaturalPersonDTO {


    @JsonProperty("EventName")
    private String eventName;

    @JsonProperty("ScreeningSubType")
    private String screeningSubType;

    @JsonProperty("AccountableCountry")
    private String accountableCountry;

    @JsonProperty("PartyId")
    private String partyId;

    @JsonProperty("PartyType")
    private String partyType;

    @JsonProperty("FirstName")
    private String firstName;

    @JsonProperty("LastName")
    private String lastName;

    @JsonProperty("PreviousFirstName")
    private String previousFirstName;

    @JsonProperty("PreviousLastName")
    private String previousLastName;

    @JsonProperty("DateOfBirth")
    private String dateOfBirth;

    @JsonProperty("Gender")
    private String gender;

    @JsonProperty("IdentificationDetails")
    private List<IdentificationDetailDTO> identificationDetails;

    @JsonProperty("Nationality")
    private String nationality;

    @JsonProperty("Address")
    private List<AddressDTO> address;

    @JsonProperty("CountryOfBirth")
    private String countryOfBirth;

    @JsonProperty("CountryOfResidence")
    private String countryOfResidence;

    @JsonProperty("Industry")
    private String industry;

    @JsonProperty("EmployeeLevel")
    private String employeeLevel;

    @JsonProperty("SourceOfIncome")
    private String sourceOfIncome;

    @JsonProperty("AdditionalSourceOfIncome")
    private String additionalSourceOfIncome;

    @JsonProperty("Contract")
    private List<ContractDTO> contract;

//    @JsonProperty("AssociatedRoles")
//    private List<AssociatedRoleDTO> associatedROles; todo find out what fields should be in here

    @JsonProperty("RiskRating")
    private RiskRatingDTO riskRating;




}
