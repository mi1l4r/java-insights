package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ContractRoleDTO {

    @JsonProperty("PartyId")
    private String partyId;

    @JsonProperty("RoleType")
    private String roleType;
}
