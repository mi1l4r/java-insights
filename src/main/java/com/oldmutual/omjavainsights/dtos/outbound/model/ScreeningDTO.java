package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ScreeningDTO {

    @JsonProperty("NaturalPerson")
    private NaturalPersonDTO naturalPerson;
}
