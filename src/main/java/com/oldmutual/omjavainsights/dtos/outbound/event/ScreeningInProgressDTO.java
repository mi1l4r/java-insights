package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ScreeningInProgressDTO {

    @JsonProperty("EventType")
    private String eventType;

    @JsonProperty("EventDate")
    private String eventDate;

    @JsonProperty("CorrelationId")
    private String correlationId;

    @JsonProperty("InitiatingSystem")
    private String initiatingSystem;

    @JsonProperty("ScreeningType")
    private String screeningType;

    @JsonProperty("AccountableCountry")
    private String accountableCountry;

    @JsonProperty("PartyId")
    private String partyId;

    @JsonProperty("PartyType")
    private String partyType;

    @JsonProperty("SanctionsOutcome")
    private String sanctionsOutcome;

    @JsonProperty("DPIPOutcome")
    private String dpipOutcome;

    @JsonProperty("FPPOOutcome")
    private String fppoOutcome;

    @JsonProperty("RCADPIPOutcome")
    private String rcadpipOutcome;

    @JsonProperty("RCAFPPOOutcome")
    private String rcafppoOutcome;

    @JsonProperty("AMEOutcome")
    private String ameOutcome;

    @JsonProperty("AMLOutcome")
    private String amlOutcome;

    @JsonProperty("StopTransaction")
    private String stopTransaction;

    @JsonProperty("ScreeningStatus")
    private String screeningStatus;

    @JsonProperty("EventId")
    private String eventId;

}
