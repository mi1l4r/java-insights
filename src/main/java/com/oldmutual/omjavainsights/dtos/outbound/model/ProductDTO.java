package com.oldmutual.omjavainsights.dtos.outbound.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ProductDTO {

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("segment")
    private String segment;

    @JsonProperty("amlproductCategoryAboveThreshold")
    private String amlProductCategoryAboveThreshold;

    @JsonProperty("amlproductCategoryBelowThreshold")
    private String amlProductCategoryBelowThreshold;
}
