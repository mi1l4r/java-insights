package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oldmutual.omjavainsights.dtos.outbound.model.ScreeningDTO;
import lombok.Data;

@Data
public class EventDTO {

    @JsonProperty("Screening")
    private ScreeningDTO screening;
}
