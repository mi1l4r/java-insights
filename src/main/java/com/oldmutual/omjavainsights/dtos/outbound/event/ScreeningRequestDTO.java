package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ScreeningRequestDTO {

    @JsonProperty("EventDate")
    private String eventDate;

    @JsonProperty("EventId")
    private String eventId;

    @JsonProperty("CorrelationId")
    private String correlationId;

    @JsonProperty("EventOwnerId")
    private String eventOwnerId;

    @JsonProperty("InitiatingSystem")
    private String initiatingSystem;

    @JsonProperty("InitiatingAccount")
    private String initiatingAccount;

    @JsonProperty("EventType")
    private String eventType;

    @JsonProperty("Event")
    private EventDTO event;
}
