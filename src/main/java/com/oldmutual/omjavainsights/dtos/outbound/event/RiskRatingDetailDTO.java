package com.oldmutual.omjavainsights.dtos.outbound.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oldmutual.omjavainsights.dtos.outbound.model.PartyDTO;
import lombok.Data;

@Data
public class RiskRatingDetailDTO {

    @JsonProperty("Party")
    private PartyDTO party;

    @JsonProperty("__DecisionID__")
    private String __decisionId__;
}
