package com.oldmutual.omjavainsights.services.event;

import com.oldmutual.omjavainsights.dtos.inbound.event.NewBusinessAPICompleteDTO;
import com.oldmutual.omjavainsights.dtos.inbound.event.NewBusinessDTO;
import com.oldmutual.omjavainsights.services.event.interfaces.INewBusinessAPICompleteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class NewBusinessAPICompleteService implements INewBusinessAPICompleteService {

    @Override
    public String processNewBusinessAPIComplete(NewBusinessDTO dto) throws InterruptedException {
        log.info("process newBusinessAPIComplete started");

        TimeUnit.SECONDS.sleep(5);

        log.info("process newBusinessAPIComplete finished");

        return dto.toString();
    }
}
