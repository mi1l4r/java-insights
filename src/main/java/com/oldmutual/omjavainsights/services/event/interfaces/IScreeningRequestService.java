package com.oldmutual.omjavainsights.services.event.interfaces;

import com.oldmutual.omjavainsights.dtos.outbound.event.ScreeningRequestDTO;
import com.oldmutual.omjavainsights.dtos.outbound.model.ScreeningDTO;

public interface IScreeningRequestService {

    //todo actual implementation of methods

    String processScreening(ScreeningRequestDTO dto) throws InterruptedException;
}
