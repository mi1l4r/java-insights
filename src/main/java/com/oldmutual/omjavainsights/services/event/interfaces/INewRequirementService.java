package com.oldmutual.omjavainsights.services.event.interfaces;

import com.oldmutual.omjavainsights.dtos.outbound.event.NewRequirementDTO;

public interface INewRequirementService {

    //todo actual implementation of methods

    String processNewRequirement(NewRequirementDTO dto) throws InterruptedException;
}
