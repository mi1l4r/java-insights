package com.oldmutual.omjavainsights.services.event.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.oldmutual.omjavainsights.dtos.outbound.event.RiskRatingResultDTO;

public interface IRiskRatingResultEventService {

    //todo actual implementation of methods
    String processRiskRatingResultEvent(Object dto) throws InterruptedException, JsonProcessingException;
}
