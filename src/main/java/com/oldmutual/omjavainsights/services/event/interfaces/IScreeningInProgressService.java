package com.oldmutual.omjavainsights.services.event.interfaces;

import com.oldmutual.omjavainsights.dtos.outbound.event.ScreeningInProgressDTO;

public interface IScreeningInProgressService {

    //todo actual implementation of methods
    String processScreeningInProgress(ScreeningInProgressDTO dto) throws InterruptedException;
}
