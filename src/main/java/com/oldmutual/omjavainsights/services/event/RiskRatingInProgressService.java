package com.oldmutual.omjavainsights.services.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.oldmutual.omjavainsights.services.conversions.RiskRatingConversions;
import com.oldmutual.omjavainsights.services.event.interfaces.IRiskRatingInProgressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class RiskRatingInProgressService implements IRiskRatingInProgressService {


    @Override
    public String processRiskRatingInProgress(Object object) throws InterruptedException, JsonProcessingException {

        log.info("process RiskRatingInProgress has started");

        var riskRatingInProgressDTO = RiskRatingConversions.convertRRInProgressToDTO(object);

        //todo actual implementation
        TimeUnit.SECONDS.sleep(5);

        log.info("process RiskRatingInProgress has finished");

        return riskRatingInProgressDTO.toString();
    }
}
