package com.oldmutual.omjavainsights.services.event;

import com.oldmutual.omjavainsights.dtos.inbound.event.ScreeningResultDTO;
import com.oldmutual.omjavainsights.services.event.interfaces.IScreeningResultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ScreeningResultService implements IScreeningResultService {


    @Override
    public String processScreeningResult(ScreeningResultDTO dto) throws InterruptedException {
        log.info("process screeningResult has started");

        TimeUnit.SECONDS.sleep(5);

        log.info("process screeningResult has finished");

        return dto.toString();
    }
}
