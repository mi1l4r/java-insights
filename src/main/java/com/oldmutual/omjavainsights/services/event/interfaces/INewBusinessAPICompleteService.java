package com.oldmutual.omjavainsights.services.event.interfaces;

import com.oldmutual.omjavainsights.dtos.inbound.event.NewBusinessAPICompleteDTO;
import com.oldmutual.omjavainsights.dtos.inbound.event.NewBusinessDTO;

public interface INewBusinessAPICompleteService {

    //todo actual implementation of Methods

    String processNewBusinessAPIComplete(NewBusinessDTO dto) throws InterruptedException;
}
