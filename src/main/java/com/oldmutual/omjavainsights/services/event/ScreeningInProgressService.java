package com.oldmutual.omjavainsights.services.event;

import com.oldmutual.omjavainsights.dtos.outbound.event.ScreeningInProgressDTO;
import com.oldmutual.omjavainsights.services.event.interfaces.IScreeningInProgressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ScreeningInProgressService implements IScreeningInProgressService {

    @Override
    public String processScreeningInProgress(ScreeningInProgressDTO dto) throws InterruptedException {

        log.info("Started process screeningInProgress");

        TimeUnit.SECONDS.sleep(5);

        log.info("Finished process screeningInProgress");
        return dto.toString();
    }
}
