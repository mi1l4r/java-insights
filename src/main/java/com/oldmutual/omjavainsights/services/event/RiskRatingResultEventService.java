package com.oldmutual.omjavainsights.services.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.oldmutual.omjavainsights.dtos.outbound.event.RiskRatingResultDTO;
import com.oldmutual.omjavainsights.services.conversions.RiskRatingConversions;
import com.oldmutual.omjavainsights.services.event.interfaces.IRiskRatingResultEventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class RiskRatingResultEventService implements IRiskRatingResultEventService {


    @Override
    public String processRiskRatingResultEvent(Object dto) throws InterruptedException, JsonProcessingException {
        log.info("process riskRatingResultEvent has started");

        var rrrEvent = RiskRatingConversions.convertRRResultToDTO(dto);

        TimeUnit.SECONDS.sleep(5);

        log.info("process riskRatingResultEvent has finished");

        return rrrEvent.toString();
    }
}
