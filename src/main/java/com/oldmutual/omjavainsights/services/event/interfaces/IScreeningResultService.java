package com.oldmutual.omjavainsights.services.event.interfaces;

import com.oldmutual.omjavainsights.dtos.inbound.event.ScreeningResultDTO;

public interface IScreeningResultService {

    //todo actual implementation
    String processScreeningResult(ScreeningResultDTO dto) throws InterruptedException;
}
