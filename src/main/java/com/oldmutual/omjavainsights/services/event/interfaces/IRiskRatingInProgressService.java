package com.oldmutual.omjavainsights.services.event.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.oldmutual.omjavainsights.dtos.outbound.event.RiskRatingInProgressDTO;


public interface IRiskRatingInProgressService {

    //Todo actual Implementation of event service methods
    String processRiskRatingInProgress(Object object) throws InterruptedException, JsonProcessingException;
}
