package com.oldmutual.omjavainsights.services.event;

import com.oldmutual.omjavainsights.dtos.outbound.event.ScreeningRequestDTO;
import com.oldmutual.omjavainsights.dtos.outbound.model.ScreeningDTO;
import com.oldmutual.omjavainsights.services.event.interfaces.IScreeningRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ScreeningRequestService implements IScreeningRequestService {


    @Override
    public String processScreening(ScreeningRequestDTO dto) throws InterruptedException {

        log.info("process processScreening Started");

        TimeUnit.SECONDS.sleep(5);

        log.info("process processScreening Finished");

        return dto.toString();
    }
}
