package com.oldmutual.omjavainsights.services.event;

import com.oldmutual.omjavainsights.dtos.outbound.event.NewRequirementDTO;
import com.oldmutual.omjavainsights.services.event.interfaces.INewRequirementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class NewRequirementService implements INewRequirementService {


    @Override
    public String processNewRequirement(NewRequirementDTO dto) throws InterruptedException {

        log.info("process NewRequirement has started");

        TimeUnit.SECONDS.sleep(5);

        log.info("process NewRequirement has ended");

        return dto.toString();
    }
}
