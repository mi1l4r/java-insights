package com.oldmutual.omjavainsights.services.event;

import com.oldmutual.omjavainsights.dtos.outbound.event.UpdateRequirementDTO;
import com.oldmutual.omjavainsights.services.event.interfaces.IUpdateRequirementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class UpdateRequirementService implements IUpdateRequirementService {


    @Override
    public String processUpdateRequirement(UpdateRequirementDTO dto) throws InterruptedException {

        log.info("process updateRequirement has started");

        TimeUnit.SECONDS.sleep(5);

        log.info("process updateRequirement has finished");

        return dto.toString();
    }
}
