package com.oldmutual.omjavainsights.services.event.interfaces;

import com.oldmutual.omjavainsights.dtos.outbound.event.UpdateRequirementDTO;

public interface IUpdateRequirementService {

    //todo actual implementation

    String processUpdateRequirement(UpdateRequirementDTO dto) throws InterruptedException;
}
