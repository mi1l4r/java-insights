package com.oldmutual.omjavainsights.services.conversions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oldmutual.omjavainsights.dtos.outbound.event.RiskRatingDetailDTO;
import com.oldmutual.omjavainsights.dtos.outbound.event.RiskRatingInProgressDTO;
import com.oldmutual.omjavainsights.dtos.outbound.event.RiskRatingResultDTO;

import java.util.Map;

public class RiskRatingConversions {

    public static RiskRatingInProgressDTO convertRRInProgressToDTO(Object object) throws JsonProcessingException {

        String escapedString = null;
        String removedEscapeString = null;
        RiskRatingDetailDTO riskRatingDetailDTO = new RiskRatingDetailDTO();

        var objMapper = new ObjectMapper();
        var jsonAsString = objMapper.writeValueAsString(object);

        Map<String, Map<String, Object>> jsonAsObject = objMapper.readValue(jsonAsString, Map.class);

        escapedString = jsonAsObject.get("RiskRatingResponse").get("RiskRatingDetail").toString();

        if ((escapedString != null || !escapedString.isEmpty()) && escapedString.contains("\\")) {

            removedEscapeString = escapedString.replace("\\", "");
            riskRatingDetailDTO = objMapper.readValue(removedEscapeString, RiskRatingDetailDTO.class);
        }

        jsonAsObject.get("RiskRatingResponse").put("RiskRatingDetail", riskRatingDetailDTO);

        return objMapper.convertValue(jsonAsObject, RiskRatingInProgressDTO.class);
    }

    public static RiskRatingResultDTO convertRRResultToDTO(Object object) throws JsonProcessingException {

        String escapedString = null;
        String removedEscapeString = null;
        RiskRatingDetailDTO riskRatingDetailDTO = new RiskRatingDetailDTO();

        var objMapper = new ObjectMapper();
        var jsonAsString = objMapper.writeValueAsString(object);

        Map<String, Map<String, Object>> jsonAsObject = objMapper.readValue(jsonAsString, Map.class);

        escapedString = jsonAsObject.get("RiskRatingResponse").get("RiskRatingDetail").toString();

        if ((escapedString != null || !escapedString.isEmpty()) && escapedString.contains("\\")) {

            removedEscapeString = escapedString.replace("\\", "");
            riskRatingDetailDTO = objMapper.readValue(removedEscapeString, RiskRatingDetailDTO.class);
        }

        jsonAsObject.get("RiskRatingResponse").put("RiskRatingDetail", riskRatingDetailDTO);

        return objMapper.convertValue(jsonAsObject, RiskRatingResultDTO.class);
    }

}
