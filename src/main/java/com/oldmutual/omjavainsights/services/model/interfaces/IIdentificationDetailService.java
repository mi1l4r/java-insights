package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.IdentificationDetail;

public interface IIdentificationDetailService {


    IdentificationDetail getIdDetailById(Long id);
}
