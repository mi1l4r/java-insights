package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.DHAVerification;

public interface IDHAVerificationService {


    DHAVerification getDHAVerificationById(Long id);
}
