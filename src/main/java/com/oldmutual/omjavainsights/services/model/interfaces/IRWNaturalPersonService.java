package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.RWNaturalPerson;

public interface IRWNaturalPersonService {


    RWNaturalPerson getRWNaturalPersonById(Long id);
}
