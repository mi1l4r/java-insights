package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.Screening;

public interface IScreeningService {


    Screening getScreeningById(Long id);
}
