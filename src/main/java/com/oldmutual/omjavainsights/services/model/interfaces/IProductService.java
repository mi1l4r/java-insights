package com.oldmutual.omjavainsights.services.model.interfaces;

import com.oldmutual.omjavainsights.model.Product;

public interface IProductService {

    Product getProductById(Long id);
}
