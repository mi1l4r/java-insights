package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.Screening;
import com.oldmutual.omjavainsights.repositories.IScreeningRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IScreeningService;
import org.springframework.stereotype.Service;

@Service
public class ScreeningService implements IScreeningService {

    private final IScreeningRepository screeningRepository;

    public ScreeningService(IScreeningRepository screeningRepository) {
        this.screeningRepository = screeningRepository;
    }

    @Override
    public Screening getScreeningById(Long id) {
        try {

            return screeningRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
