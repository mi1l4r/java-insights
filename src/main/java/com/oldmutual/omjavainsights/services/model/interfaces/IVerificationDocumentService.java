package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.VerificationDocument;

public interface IVerificationDocumentService {


    VerificationDocument getVerDocById(Long id);
}
