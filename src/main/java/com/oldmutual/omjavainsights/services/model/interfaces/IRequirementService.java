package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.Requirement;

public interface IRequirementService {


    Requirement getRequirementById(Long id);
}
