package com.oldmutual.omjavainsights.services.model.interfaces;

import com.oldmutual.omjavainsights.model.BusinessTransaction;
import com.oldmutual.omjavainsights.model.Contract;
import com.oldmutual.omjavainsights.model.Party;

import java.util.List;

public interface IPartyService {

    Party getPartyById(String id);

    List<Party> getPartiesByTransaction(BusinessTransaction businessTransaction);

    List<Party> getPartiesByContract(Contract contract);
}
