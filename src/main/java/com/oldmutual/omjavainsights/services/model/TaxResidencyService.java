package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.TaxResidency;
import com.oldmutual.omjavainsights.repositories.ITaxResidencyRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.ITaxResidencyService;
import org.springframework.stereotype.Service;

@Service
public class TaxResidencyService implements ITaxResidencyService {

    private final ITaxResidencyRepository taxResidencyRepository;

    public TaxResidencyService(ITaxResidencyRepository taxResidencyRepository) {
        this.taxResidencyRepository = taxResidencyRepository;
    }


    @Override
    public TaxResidency getTaxResidencyById(Long id) {
        try {

            return taxResidencyRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
