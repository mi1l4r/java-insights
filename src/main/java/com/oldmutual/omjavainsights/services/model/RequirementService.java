package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.Requirement;
import com.oldmutual.omjavainsights.repositories.IRequirementRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IRequirementService;
import org.springframework.stereotype.Service;

@Service
public class RequirementService implements IRequirementService {

    private final IRequirementRepository requirementRepository;

    public RequirementService( IRequirementRepository requirementRepository) {
        this.requirementRepository = requirementRepository;
    }


    @Override
    public Requirement getRequirementById(Long id) {
        try {

           return requirementRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
