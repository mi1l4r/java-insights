package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.Role;

public interface IRoleService {


    Role getRoleById(Long id);
}
