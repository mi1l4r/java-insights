package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.Contract;
import com.oldmutual.omjavainsights.repositories.IContractRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IContractService;
import org.springframework.stereotype.Service;

@Service
public class ContractService implements IContractService {

    private final IContractRepository contractRepository;


    public ContractService(IContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    @Override
    public Contract getContractById(Long id) {

        try {

            return contractRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }

    }

}
