package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.AssociatedParty;
import com.oldmutual.omjavainsights.repositories.IAssociatedPartyRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IAssociatedPartyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@Slf4j
public class AssociatedPartyService implements IAssociatedPartyService {

    private final IAssociatedPartyRepository associatedPartyRepository;

    public AssociatedPartyService(IAssociatedPartyRepository associatedPartyRepository) {
        this.associatedPartyRepository = associatedPartyRepository;
    }

    @Override
    public AssociatedParty getAssociatedPartyById(Long id) throws Exception {

        log.info("Method getAssociatedPartyById started");

        AssociatedParty associatedParty;

        try{

            associatedParty = associatedPartyRepository.findById(id).orElseThrow(
                    () -> new NoSuchElementException("No AssociatedParty with id: " + id + " was found")
            );

        }catch (NoSuchElementException e){

            log.error("NoSuchElementException: "+e.getMessage()+" in AssociatedPartyService.getAssociatedPartyById");
            throw new NoSuchElementException(e.getMessage());

        }catch (Exception e){

            log.error("Unknown Exception occurred in AssociatedPartyService.getAssociatedPartyById");
            throw new Exception(e.getMessage(),e.getCause());
        }

        log.info("Method getAssociatedPartyById finished");

        return associatedParty;
    }
}
