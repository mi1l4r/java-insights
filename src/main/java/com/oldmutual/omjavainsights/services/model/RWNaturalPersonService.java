package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.RWNaturalPerson;
import com.oldmutual.omjavainsights.repositories.IRWNaturalPersonRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IRWNaturalPersonService;
import org.springframework.stereotype.Service;

@Service
public class RWNaturalPersonService implements IRWNaturalPersonService {

    private final IRWNaturalPersonRepository rwNaturalPersonRepository;

    public RWNaturalPersonService(IRWNaturalPersonRepository rwNaturalPersonRepository) {
        this.rwNaturalPersonRepository = rwNaturalPersonRepository;
    }

    @Override
    public RWNaturalPerson getRWNaturalPersonById(Long id) {
        try {

            return rwNaturalPersonRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
