package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.ContractRole;
import com.oldmutual.omjavainsights.repositories.IContractRoleRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IContractRoleService;
import org.springframework.stereotype.Service;

@Service
public class ContractRoleService implements IContractRoleService {

    private final IContractRoleRepository contractRoleRepository;

    public ContractRoleService(IContractRoleRepository contractRoleRepository) {
        this.contractRoleRepository = contractRoleRepository;
    }


    @Override
    public ContractRole getContractRoleById(Long id) {
        try {

            return contractRoleRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
