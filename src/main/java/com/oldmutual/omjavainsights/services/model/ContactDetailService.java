package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.ContactDetail;
import com.oldmutual.omjavainsights.repositories.IContactDetailRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IContactDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@Slf4j
public class ContactDetailService implements IContactDetailService {

    private final IContactDetailRepository contactDetailRepository;

    public ContactDetailService(IContactDetailRepository contactDetailRepository) {
        this.contactDetailRepository = contactDetailRepository;
    }

    @Override
    public ContactDetail getContactDetailById(Long id) throws Exception {
        log.info("Method getContactDetailById started");

        ContactDetail contactDetail;

        try {

            contactDetail = contactDetailRepository.findById(id).orElseThrow(
                    () -> new NoSuchElementException("No ContactDetail with id: " + id + " was found")
            );

        } catch (NoSuchElementException e) {

            log.error("NoSuchElementException: " + e.getMessage() + " in ContactDetailService.getContactDetailById");
            throw new NoSuchElementException(e.getMessage());

        } catch (Exception e) {

            log.error("Unknown Exception occurred in ContactDetailService.getContactDetailById");
            throw new Exception(e.getMessage(), e.getCause());
        }

        log.info("Method getAssociatedPartyById finished");

        return contactDetail;

    }
}
