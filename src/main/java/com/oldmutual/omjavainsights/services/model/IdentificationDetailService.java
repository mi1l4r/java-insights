package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.IdentificationDetail;
import com.oldmutual.omjavainsights.repositories.IIdentificationDetailRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IIdentificationDetailService;
import org.springframework.stereotype.Service;

@Service
public class IdentificationDetailService implements IIdentificationDetailService {

    private final IIdentificationDetailRepository identificationDetailRepository;

    public IdentificationDetailService( IIdentificationDetailRepository identificationDetailRepository) {
        this.identificationDetailRepository = identificationDetailRepository;
    }

    @Override
    public IdentificationDetail getIdDetailById(Long id) {

        try {

            return identificationDetailRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
