package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.Address;

public interface IAddressService {

    Address getAddressById(Long id) throws Exception;
}
