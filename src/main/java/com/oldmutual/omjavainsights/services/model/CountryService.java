package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.Country;
import com.oldmutual.omjavainsights.repositories.ICountryRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.ICountryService;
import org.springframework.stereotype.Service;

@Service
public class CountryService implements ICountryService {

    private final ICountryRepository countryRepository;

    public CountryService(ICountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Country getCountryById(Long id) {
        try {

            return countryRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
