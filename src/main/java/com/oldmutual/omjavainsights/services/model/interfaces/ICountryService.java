package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.Country;

public interface ICountryService {

    Country getCountryById(Long id);
}
