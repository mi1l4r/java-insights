package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.Product;
import com.oldmutual.omjavainsights.repositories.IProductRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IProductService;
import org.springframework.stereotype.Service;

@Service
public class ProductService implements IProductService {

    private final IProductRepository productRepository;

    public ProductService( IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product getProductById(Long id) {
        try {

            return productRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
