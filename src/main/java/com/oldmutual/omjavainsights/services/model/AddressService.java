package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.Address;
import com.oldmutual.omjavainsights.repositories.IAddressRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IAddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@Slf4j
public class AddressService implements IAddressService {

    private final IAddressRepository addressRepository;

    public AddressService(IAddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Address getAddressById(Long id) throws Exception {

        log.info("Method getAddressById started");

        Address address;

        try{

            address = addressRepository.findById(id).orElseThrow(
                    () -> new NoSuchElementException("No Address with id: " + id + " was found")
            );

        }catch (NoSuchElementException e){

            log.error("NoSuchElementException: "+e.getMessage()+" in AddressService.getAddressById");
            throw new NoSuchElementException(e.getMessage());

        }catch (Exception e){

            log.error("Unknown Exception occurred in AddressService.getAddressById");
            throw new Exception(e.getMessage(),e.getCause());
        }

        log.info("Method getAddressById finished");

        return address;

    }
}
