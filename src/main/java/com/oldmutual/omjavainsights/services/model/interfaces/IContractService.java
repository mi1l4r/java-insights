package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.Contract;

public interface IContractService {


    Contract getContractById(Long id);
}
