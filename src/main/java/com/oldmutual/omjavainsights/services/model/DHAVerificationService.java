package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.DHAVerification;
import com.oldmutual.omjavainsights.repositories.IDHAVerificationRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IDHAVerificationService;
import org.springframework.stereotype.Service;

@Service
public class DHAVerificationService implements IDHAVerificationService {

    private final IDHAVerificationRepository verificationRepository;

    public DHAVerificationService(IDHAVerificationRepository verificationRepository) {
        this.verificationRepository = verificationRepository;
    }

    @Override
    public DHAVerification getDHAVerificationById(Long id) {
        try {

            return verificationRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
