package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.RequirementsWrapper;

public interface IRequirementsWrapperService {


    RequirementsWrapper getRWById(Long id);

}
