package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.BusinessTransaction;
import com.oldmutual.omjavainsights.model.Contract;
import com.oldmutual.omjavainsights.model.Party;
import com.oldmutual.omjavainsights.repositories.IPartyRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IPartyService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartyService implements IPartyService {

    private final IPartyRepository partyRepository;

    public PartyService(IPartyRepository partyRepository) {
        this.partyRepository = partyRepository;
    }




    @Override
    public Party getPartyById(String id) {

        try {

            return partyRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }

    }

    @Override
    public List<Party> getPartiesByTransaction(BusinessTransaction businessTransaction) {

        var parties =  partyRepository.findByBusinessTransactions(businessTransaction);

        if(parties.size() == 0){
            return null;
        }

        return parties;
    }

    @Override
    public List<Party> getPartiesByContract(Contract contract) {
        var parties = partyRepository.findByBusinessTransactions_Contracts(contract);

        if(parties.size() == 0) {
            return null;
        }

        return parties;
    }


}
