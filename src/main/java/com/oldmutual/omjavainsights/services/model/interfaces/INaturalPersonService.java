package com.oldmutual.omjavainsights.services.model.interfaces;

import com.oldmutual.omjavainsights.model.NaturalPerson;

public interface INaturalPersonService {


    NaturalPerson getNaturalPersonById(Long id);
}
