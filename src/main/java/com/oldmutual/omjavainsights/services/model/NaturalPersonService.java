package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.NaturalPerson;
import com.oldmutual.omjavainsights.repositories.INaturalPersonRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.INaturalPersonService;
import org.springframework.stereotype.Service;

@Service
public class NaturalPersonService implements INaturalPersonService {

    private final INaturalPersonRepository naturalPersonRepository;

    public NaturalPersonService(INaturalPersonRepository naturalPersonRepository) {
        this.naturalPersonRepository = naturalPersonRepository;
    }

    @Override
    public NaturalPerson getNaturalPersonById(Long id) {

        try {

            return naturalPersonRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
