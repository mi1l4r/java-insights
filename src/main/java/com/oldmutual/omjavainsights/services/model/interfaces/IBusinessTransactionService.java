package com.oldmutual.omjavainsights.services.model.interfaces;

import com.oldmutual.omjavainsights.model.BusinessTransaction;
import com.oldmutual.omjavainsights.model.Contract;
import com.oldmutual.omjavainsights.model.Party;

import java.util.List;

public interface IBusinessTransactionService {

    BusinessTransaction getBusinessTransactionById(Long id) throws Exception;

    List<BusinessTransaction> getBusinessTransactionsByParty(Party party) throws Exception;

    List<BusinessTransaction> getBusinessTransactionsByContract(Contract contract) throws Exception;
}
