package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.ContractRole;

public interface IContractRoleService {

    ContractRole getContractRoleById(Long id);
}
