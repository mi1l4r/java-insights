package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.VerificationDocument;
import com.oldmutual.omjavainsights.repositories.IVerificationDocumentRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IVerificationDocumentService;
import org.springframework.stereotype.Service;

@Service
public class VerificationDocumentService implements IVerificationDocumentService {

    private final IVerificationDocumentRepository verificationDocumentRepository;

    public VerificationDocumentService(IVerificationDocumentRepository verificationDocumentRepository) {
        this.verificationDocumentRepository = verificationDocumentRepository;
    }

    @Override
    public VerificationDocument getVerDocById(Long id) {
        try {

            return verificationDocumentRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
