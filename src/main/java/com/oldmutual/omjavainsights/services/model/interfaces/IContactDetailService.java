package com.oldmutual.omjavainsights.services.model.interfaces;


import com.oldmutual.omjavainsights.model.ContactDetail;

public interface IContactDetailService {

    ContactDetail getContactDetailById(Long id) throws Exception;
}
