package com.oldmutual.omjavainsights.services.model.interfaces;

import com.oldmutual.omjavainsights.model.AssociatedParty;

public interface IAssociatedPartyService {

    /**
     * Todo
     * - Implementation Of Methods needed for AssociatedParty service interface
     */

    //Fake method made for testing
    AssociatedParty getAssociatedPartyById(Long id) throws Exception;
}
