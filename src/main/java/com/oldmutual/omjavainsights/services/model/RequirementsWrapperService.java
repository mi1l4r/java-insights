package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.RequirementsWrapper;
import com.oldmutual.omjavainsights.repositories.IRequirementsWrapperRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IRequirementsWrapperService;
import org.springframework.stereotype.Service;

@Service
public class RequirementsWrapperService implements IRequirementsWrapperService {

    private final IRequirementsWrapperRepository requirementsWrapperRepository;

    public RequirementsWrapperService(IRequirementsWrapperRepository requirementsWrapperRepository) {

        this.requirementsWrapperRepository = requirementsWrapperRepository;
    }

    @Override
    public RequirementsWrapper getRWById(Long id) {
        try {

            return requirementsWrapperRepository.findById(id).get();
        } catch (Exception e) {

            //todo implement logging
            return null;
        }
    }
}
