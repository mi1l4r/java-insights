package com.oldmutual.omjavainsights.services.model;

import com.oldmutual.omjavainsights.model.BusinessTransaction;
import com.oldmutual.omjavainsights.model.Contract;
import com.oldmutual.omjavainsights.model.Party;
import com.oldmutual.omjavainsights.repositories.IBusinessTransactionRepository;
import com.oldmutual.omjavainsights.services.model.interfaces.IBusinessTransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@Slf4j
public class BusinessTransactionService implements IBusinessTransactionService {

    private final IBusinessTransactionRepository businessTransactionRepository;


    public BusinessTransactionService(IBusinessTransactionRepository businessTransactionRepository) {
        this.businessTransactionRepository = businessTransactionRepository;
    }


    @Override
    public BusinessTransaction getBusinessTransactionById(Long id) throws Exception {

        log.info("Method getBusinessTransactionById started");

        BusinessTransaction businessTransaction;

        try{

            businessTransaction = businessTransactionRepository.findById(id).orElseThrow(
                    () -> new NoSuchElementException("No BusinessTransaction with id: " + id + " was found")
            );

        }catch (NoSuchElementException e){

            log.error("NoSuchElementException: "+e.getMessage()+" in BusinessTransactionService.getBusinessTransactionById");
            throw new NoSuchElementException(e.getMessage());

        }catch (Exception e){

            log.error("Unknown Exception occurred in BusinessTransactionService.getBusinessTransactionById");
            throw new Exception(e.getMessage(),e.getCause());
        }

        log.info("Method getAssociatedPartyById finished");

        return businessTransaction;

    }

    @Override
    public List<BusinessTransaction> getBusinessTransactionsByParty(Party party) throws Exception {

        log.info("Method getBusinessTransactionsByParty started");

        List<BusinessTransaction> businessTransactions;

        try{

            businessTransactions = businessTransactionRepository.findByParties(party);

        }catch (Exception e){

            log.error("Unknown Exception occurred in BusinessTransactionService.getBusinessTransactionsByParty");
            throw new Exception(e.getMessage(), e.getCause());
        }

        return businessTransactions;

    }

    @Override
    public List<BusinessTransaction> getBusinessTransactionsByContract(Contract contract) throws Exception {

        log.info("Method getBusinessTransactionsByParty started");

        List<BusinessTransaction> businessTransactions;

        try{
            businessTransactions = businessTransactionRepository.findByContracts(contract);
        }catch (Exception e){
            log.error("Unknown Exception occurred in BusinessTransactionService.getBusinessTransactionsByContract");
            throw new Exception(e.getMessage(),e.getCause());
        }

         return businessTransactions;
    }
}
