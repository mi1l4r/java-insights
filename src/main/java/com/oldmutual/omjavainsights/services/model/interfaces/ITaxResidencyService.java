package com.oldmutual.omjavainsights.services.model.interfaces;

import com.oldmutual.omjavainsights.model.TaxResidency;

public interface ITaxResidencyService {


    TaxResidency getTaxResidencyById(Long id);
}
